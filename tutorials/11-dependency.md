# Závislosti projektu - cvičení

> [Teoretická část](../11-dependency.md)

## `npm` a `Bower`

- Pomocí `Bower` nainstalujte framework [Bootstrap](https://getbootstrap.com/) a upravte zdrojový kód, tak aby byl použit pro UI
  - Načtěte CSS
  - Upravte CSS třídy pro tlačítka, formulářové prvky apod.

## `gulp`

- Pomocí `npm` nainstalujte `gulp` a vytvořte úlohy
  - Pro spojení a minifikaci javascriptových souborů
  - Pro spojení a minifikaci kaskádových stylů
  - Společnou úlohu pro spuštění obou předchozích
  - Použijte pluginy `gulp-uglify`, `gulp-concat`, `gulp-minify-css`
  - Prozkoumejte další možnosti využití `gulp` nebo `Grunt` pro Vaši aplikaci
  
## Odkazy

- [npm](https://docs.npmjs.com/)
- [Bower](https://bower.io/)
- [gulp](http://gulpjs.com/)
- [Grunt](http://gruntjs.com/)