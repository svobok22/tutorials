# Základy Javascriptu - cvičení

> [Teoretická část](../01-basics.md)

## Kde zkoušet Javascript?

- `node` (*node.js*)
  - interaktivní konzole
  - neinteraktivně `node soubor.js`
- konzole webového prohlížeče

## Nutný začátek

```javascript
console.log('Hello world!');
```

## Proměnné

- Jaký bude výsledek? Jak získat součet? 

```javascript
var a = '10', b = 5;
var c = a + b, d = b + a;
```

## Funkce

1. Napište funkci, která vypočte `n`-tou mocninu čísla `x`
1. Napište funkci, která spočítá součet všech svých argumentů

viz [if/else](../03-operators-expressions.md#v%C4%9Btven%C3%AD-if-else), [for](../03-operators-expressions.md#cyklus-for) cyklus

## Řídicí struktury

1. Napište funkci, která určí, zda načtená hodnota je číslo a pokud ano, tak vypíše zda je záporné, či nezáporné.
1. Napište funkci, ktera zjistí, zda je zadaný rok přestupný. Použijte Gregoriánský kalendář platný od září 1584, ve kterém platí:
   - Je-li rok dělitelný 4, pak je přestupný,
   - je-li rok dělitelný 100, pak není přestupný,
   - je-li rok dělitelný 400, pak je přestupný.
1. **Předchozí příklad napište pomocí jediného logického výrazu.**
1. Napište funkci, která vypíše prvních deset násobků zadaného čísla.
1. Napište funkci, která vypíše všechny násobky zadaného čísla menší než 100.
1. **Napiště rekurzivní a iterativní výpočet faktoriálu. Vyzkoušejte si algoritmus krokovat a porovnat složitost.**

## Datové struktury

1. Napište funkci, která naplní pole náhodnými hodnotami.
1. Napište funkci, která pole seřadí a nečíselné hodnoty z pole odstraní.
1. **Napište funkci, která v poli uspořádá čísla tak, aby všechna sudá byla od začátku a lichá byla na konci pole.**