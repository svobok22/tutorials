# Závislosti projektu - Bower, Gulp, Grunt

## `npm`

- správce závislostí (balíčků) pro *node.js*
- manifest a seznam závislostí v `package.json`
- balíčky v adresáři `node_modules`

## vlastnosti `npm` 

- široká funkcionalita
- možnost instalace balíčků lokálně nebo globálně `-g`
- spouštění vlastních skriptů při vývoji
  - lokální webserver
  - minifikace
  - testy
- instalace balíčku je pomalá → `yarn`

## použití `npm`

- `npm init` - vytvoření `package.json`
- `npm install` - instalace závislostí (lze také použít zkraceně `npm i`)
- `npm i X@Y` - instalace závislosti `X` ve verzi `Y`
- `npm install X --save` - instalace závislosti `X` s uložením do `package.json`
- `npm run ...` - spouštění skriptů
- `npm install X -g` - globální instalace závislosti `X`

## `Yarn`

- stejně jako `npm` to je správce závislostí (balíčků) pro *node.js*
- manifest a seznam zavislosti je také v `package.json`, závislosti jsou nainstalovány také v `node_modules`
- kdykoliv je modul přidán, yarn vytvoří nebo aktualizuje `yarn.lock`, což je analogie k `npm-shrinkwrap.json`
- velká výhoda `yarnu` oproti `npm` je paralelní instalace závislostí

## použití `Yarn`

- `yarn init` - vytvoření `package.json`
- `yarn add X` - instalace závislosti `X` rovnou přidá do `package.json` údaj o balíčku - analogie k `npm i X --save`
- `yarn add X@Y` - instalace závislosti `X` ve verzi `Y`
- `yarn run ...` - spouštění skriptů
- `yarn global install X` - v `Yarn` se řeší prefixem a ne `--global` flagem jako v `npm`

## Bower

- správce závislostí (balíčků)
- manifest a seznam závislostí v `bower.json`
- balíčky v adresáři `bower_components`

## vlastnosti Bower

- neprovádí žádné další operace (minifikaci apod.)
- instalace

```javascript
npm install bower
```

## použití Bower

- `bower init` - vytvoření `bower.json`
- `bower install` - instalace závislostí
- `bower install X --save` - instalace závislosti X s uložením do `bower.json`

## Zásadní otázka

- [What is the difference between Bower and npm?](https://stackoverflow.com/questions/18641899/what-is-the-difference-between-bower-and-npm)

## Gulp

- automatizace rutinních činností při vývoji
- *task runner*
- konfigurace úloh v `gulpfile.js`
- instalace

```bash
npm install gulp gulp-cli -D
touch gulpfile.js
```

## použití `gulp`

- v `gulpfile.js` jsou definovány úlohy (task), které lze spouštět pomocí

```bash
gulp název
```

- pluginy - minifikace JS a CSS

```bash
npm install gulp-concat gulp-rename gulp-uglify gulp-minify-css -D
```

- pluginy se načítají pomocí `require()`
- vstup/výstup jednotlivých kroků úlohy se řetězí pomocí `pipe()`

```javascript
var gulp = require('gulp');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');

gulp.task('min', function() {
    return gulp.src('js/script.js')
        .pipe(rename('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'))
    ;
});
```

## Grunt

- další *task runner*
- konfigurace v `Gruntfile.js` (nebo `Gruntfile.coffee`)
- instalace

```bash
npm install grunt grunt-cli -D
```

## použítí Grunt

- v `Gruntfile.js` je konfigurace a definice úloh, spuštění

```bash
grunt název
```

- pluginy - minifikace JS a CSS

```bash
npm install grunt-contrib-uglify grunt-contrib-cssmin -D
```

- ruční vytvoření `Gruntfile.js`

```javascript
module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.registerTask('default', ['uglify']);
};
```

## gulp vs Grunt

- sledování souborů (watch) je v gulp vestavěno, Grunt potřebuje plugin
- gulp pluginy dělají jednu věc, Grunt pluginy jsou komplexnější
- gulp úlohy jsou JavaScript, Grunt úlohy jsou JSON
- gulp podporuje proudové zpracování, Grunt ukládá mezi jednotlivými kroky

## Zdroje

- [npm](https://docs.npmjs.com/)
- [Bower](https://bower.io/)
- [gulp](http://gulpjs.com/)
- [Grunt](http://gruntjs.com/)
- [What is the difference between Bower and npm?](https://stackoverflow.com/questions/18641899/what-is-the-difference-between-bower-and-npm)
- [Concatenate & Minify Javascript with Gulp, Improve Site Performance](http://codehangar.io/concatenate-and-minify-javascript-with-gulp/)
- [An Introduction to Gulp.js](https://www.sitepoint.com/introduction-gulp-js/)