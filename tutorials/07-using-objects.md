# Použití OOP, serializace - cvičení

> [Teoretická část](../07-using-objects.md)

Metody vytvořte do samostatného souboru (stejně jako modely), který budete načítat do hlavního programu

- Implementujte metodu pro uživatelsky přívětivý výpis úkolů zadaného uživatele např. v syntaxi *Markdown*

```
# user1@example.com
## General
- [x] Finish thesis
- [ ] Celebrate
## Shopping
- [x] Pastry
- [ ] Butter
```

- Implementujte metodu pro *serializaci* dat (na úrovni jednoho uživatele)
  - kvůli vzájemným vazbám nelze rovnou využít `JSON.stringify`
  - nahraďte vnořené objekty jejich ID
- Implementuje metodu pro *deserializaci* dat
  - tato metoda rekonstruuje ID na odkazy na objekty
- Ověřte, že objekty vzniklé po deserializaci jsou totožné a nejde o různé se stejnými hodnotami atributů
  (viz kontrola konzistence v minulém cvičení)

## Dědičnost

- Vytvořte nový model `ShoppingTask` (položka nákupního seznamu), který dědí od `Task` a přidává nový atribut `amount` pro množství