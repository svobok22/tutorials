function addDiv(root) {
    // vytvoreni elementu
    var el = document.createElement('div');
    el.innerHTML = Math.random();
    // vlozeni potomka
    root.appendChild(el);
}

function removeDiv(root) {
    // firstElementChild misto firstChild - jinak by se smazal textovy uzel 'B'
    var el = root.firstElementChild;
    if(el) {
        // odstraneni potomka
        root.removeChild(el);
    }
}

window.addEventListener('load', function() {
    document.getElementById('buttonAdd').addEventListener('click', function() {
        addDiv(document.getElementById('B'));
    });

    document.getElementById('buttonRemove').addEventListener('click', function() {
        removeDiv(document.getElementById('B'));
    });
});