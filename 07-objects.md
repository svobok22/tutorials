# Základy OOP

## Třídní vs. Prototypový
- třídy + instance vs. objekty
- prototyp = šablona pro další objekt
- přidání/odstranění atributu za běhu (i do prototypu)

## Vytvoření objektu - konstruktor

```javascript
var o = new Object();
o.name = 'Alois';
o['id'] = 12345;
```
- tečková notace nebo přístup jako k poli

## Vytvoření objektu - konstruktor
- *constructor function*

```javascript
function Person(name, dept, area) {
    this.name = name;
    this.department = {};
    this.department.name = dept;
    this.department.area = area;
}
var o = new Person('Alois', 'Management', 'EMEA');
```

## Vytvoření objektu - literál
- *object initializer*

```javascript
var o = {
    name: 'Alois',
    department: { name: 'Management', area: 'EMEA' }
};
o.name                    // 'Alois'
o.department.area         // 'EMEA'
o['department'].area      // 'EMEA'
o.department['area']      // 'EMEA'
o['department']['area']   // 'EMEA'
```

## Vytvoření objektu - metoda `create`
- `Object.create(prototype [, propObject])`
- umožnuje určit prototyp bez nutnosti *constructor function*
- tzn. vytvoří objekt na základě jiného

```javascript
var Person = {
    name: 'Osoba',
    department: { name: 'Management', area: 'global' },
    getDepartment: function() { return this.department.name + '/' + this.department.area; }
};
var alois = Object.create(Person);
console.log(alois.getDepartment());   // 'Management/global'
```

## Vytvoření objektu - literál
- od ECMAScript 6:

```javascript
var x = 5;
var obj = {
    // zkratka za 'x: x'
    x,
    // metoda
    toString() {
     // volání rodiče
     return "d " + super.toString();
    },
    // dynamický název atributu 'attr42: 42'
    [ 'attr' + (40 + 2) ]: 42
};
```

## Vytvoření objektu - klíčové slovo `class`
- od ECMAScript 6, jazyk je stále prototypový
- klíčová slova `class`, `constructor`, `static`, `extends`, `super`

```javascript
"use strict"

class Employee {
    constructor(name, salary) {
        this.name = name;
        this.salary = salary;
    }
}

class Manager extends Employee {
    constructor(salary, workers) {
        super('Boss', salary);
        this.workers = workers;
    }
}
```

## Iterování objektu
- `for of`
- `Object.keys(o)` nebo `Object.getOwnPropertyNames(o)` (vrací i "non-enumerable" atributy)

```javascript
for (var prop of Object.keys(o)) {
    console.log(prop, '=', o[prop]);
}
```

## Dědičnost atributů

```javascript
var Person = {
    name: 'Osoba',
    department: { name: 'Management', area: 'global' },
    getNick: function() { return '@' + this.name; },
    getDepartment: function() { return this.department.name + '/' + this.department.area; }
};
var alois = Object.create(Person);
```
- objekt `alois` zatím nemá žádné vlastní atributy (vlastnosti)

```javascript
console.log(alois);        // {}
console.log(alois.name);   // 'Osoba' - z prototypu Person
alois.name = 'Alois';
console.log(alois);        // { name: 'Alois' }
console.log(alois.name);   // 'Alois' - vlastní
console.log(alois.__proto__.name); // 'Osoba'
```

## Dědičnost atributů
![](img/06-inheritance.dot.png) ![](img/06-inheritance2.dot.png)

## Dědičnost atributů - vnořených objektů
- vnořený `Person.department` objekt je děden jako reference

```javascript
alois.department.area = 'EMEA'; // přes referenci přistupujeme do objektu
console.log(alois.department);  // { name: 'Management', area: 'EMEA' }
console.log(Person.department); // { name: 'Management', area: 'EMEA' } !
alois.department = 100;         // teď se reference přepíše
console.log(alois.department);  // 100
console.log(Person.department); // { name: 'Management', area: 'EMEA' } !
```

## Dědičnost metod

```javascript
console.log(Person.getNick());                // '@Osoba'
console.log(Person.getDepartment());          // 'Management/global'
```
- `this` v metodě odkazuje na potomka, nikoliv na prototyp

```javascript
alois.name = 'Alois';
console.log(alois.getNick());                 // '@Alois'
console.log(alois.__proto__.getNick());       // '@Osoba'
```

## Přístup k prototypu (rodiči)

```javascript
console.log(alois.__proto__.name);                // 'Osoba'
console.log(Object.getPrototypeOf(alois).name);   // 'Osoba'
```

## Úprava prototypu

```javascript
function Employee(name) {
    this.name = name;
}
var cyril = new Employee('Cyril');
Employee.prototype.salary = 5000;   // totéž co cyril.__proto__.salary = 5000;
console.log(cyril);                 // Employee { name: 'Cyril' }
console.log(cyril.salary);          // 5000
var david = new Employee('David');
console.log(david.salary);          // 5000
Employee.bonus = 10000;
console.log(cyril.bonus);           // undefined
console.log(Employee.salary);       // undefined
console.log(Employee.bonus);        // 10000
console.log(Employee);              // { [Function: Employee] bonus: 10000 }
```

## Úprava prototypu
![](img/06-updating-prototype.dot.png) ![](img/06-updating-prototype2.dot.png)

## Úprava prototypu

```javascript
var Employee = { name: 'Zamestnanec' };
var cyril = Object.create(Employee);
cyril.name = 'Cyril';
var david = Object.create(Employee);
Employee.salary = 5000;             // totéž co cyril.__proto__.salary = 5000;
console.log(cyril.salary);          // 5000
console.log(david.salary);          // 5000
```

## Úprava prototypu
![](img/06-updating-prototype3.dot.png)

## Získání vlastních atributů
- iterace pouze přes vlastní atributy

```javascript
for (var prop of Object.keys(alois)) {
    console.log(prop, '=', alois[prop]);
}
```
```
name = Alois
```
- poznámka: `console.log` umožňuje více argumentů, takto se `alois.department` vypíše čitelně narozdíl od `console.log(prop + '=' + o[prop]);`

## Získání všech atributů
- atributy z prototypu můžeme získat pomocí iterace `for in`

```javascript
for (var prop in alois) {
    console.log(prop, '=', alois[prop]);
}
```
```
name = Alois
department = { name: 'Management', area: 'global' }
getDepartment = function () { return this.department.name + '/' + this.department.area; }
```

## gettery a settery
- s funkcí `a` se pracuje jako s atributem
- použití `a` uvnitř getteru/setteru `a()` může způsobit zacyklení, proto `_a`

```javascript
var o = {
    _a: 1,
    get a() { return this._a + 3; }
    set a(x) { this._a = x / 4; }
}
console.log(o.a);    // 4
o.a = 20;
console.log(o.a);    // 8
console.log(o._a);   // 5
```

## Odstranění atributu

```javascript
var o = {
    a: 10,
    b: 'Ahoj'
};
delete o.b;
console.log(o);      // { a: 10 }
```

## Porovnávání objektů
- `false` i když má objekt stejné hodnoty atributů

```javascript
var o = { a: 22 };
var p = { a: 22 };
console.log(o == p);    // false
console.log(o === p);   // false
```

- `true` pokud jde o stejnou referenci

```javascript
var q = o;
console.log(o == q);   // true
console.log(o === q);  // true
```

opakování:

```javascript
q.a = 5;
console.log(o.a);      // 5
```

## Zdroje

- [MDN Working with objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Working_with_Objects)
- [MDN Inheritance and the prototype chain](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Inheritance_and_the_prototype_chain)
- [MDN Details of the object model](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Details_of_the_Object_Model)
- [MDN new operator details](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Details_of_the_Object_Model#Inheriting_properties)
- [Javascript. The core.](http://dmitrysoshnikov.com/ecmascript/javascript-the-core/)
