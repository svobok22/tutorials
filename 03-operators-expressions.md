# Operátory

## Typy operátorů (a)

- porovnání `==`, `!=`, striktní `===`, `!==` (vč. typu), `>`, `>=`, `<`, `<=`
- aritmetické `%`, `++`, `--`, `-`, `+`, exponent `**`
- bitové `&`, `|`, xor `^`, not `~`, posun `<<`, `>>`, `>>>` (nasouvání 0 zleva)
- logické `&&`, `||`, negace `!`
- unární `delete`, `typeof`, `void` (výraz nevrací hodnotu `void x`)
  
  ```javascript
  x = 10        // 10
  void (x = 10)   // undefined
  ```
- ternární `x ? a : b`
- `,` vrátí hodnotu posledního operandu (`a, b, c`)
- relační `in`, `instanceof`

  ```javascript
  o = { a: 10, b: 20 }, p = ['a','b']
  'a' in o   // true
  10  in o   // false - hledá se podle atributu ne obsahu
  1   in p   // true
  'a' in p   // false - hledá se podle klíče (indexu) ne obsahu
  ```
## Typy operátorů (b)

- přiřazení `=`
- zkrácené operátory
aritmetické `x += y`, `x -= y`, `x *= y`, `x /= y`, `x %= y`, `x **= y`,
bitový posun `x <<= y`, `x >>= y`, `x >>>= y`,
bitové `x &= y`, `x ^= y`, `x |= y`

## Porovnání `==` vs `===`

```javascript
console.log(5 == '5')   // true
console.log(1 == true)  // true
console.log(5 === '5')  // false
```

```javascript
var o = { a: 22 };
var p = { a: 22 };
console.log(o == p);    // false
console.log(o === p);   // false
var q = o;
console.log(o == q);    // true
console.log(o === q);   // true
```

## Zkrácené vyhodnocení

```javascript
false && x == false
 true || x == true
```

`x` se nevyhodnocuje

## Spread operátor

- `...`

```javascript
var a = [ 2, 3, 4 ];
var b = [ 1, ...a, 5, 6];

function add(a, b) { return a + b; }
var args = [ 1, 2 ];
var sum = add(...args);
```

## Priorita operátorů

- člen	`.` `[]`
- volání, instanciace	`()` `new`
- negace, inkrementace	`!` `~` `-` `+` `++` `--` `typeof` `void` `delete`
- násobení, dělení	`*` `/` `%`
- sčítání, odčítání	`+` `-`
- bitový posun	`<<` `>>` `>>>`
- porovnání	`<` `<=` `>` `>=` `in` `instanceof`
- rovnost	`==` `!=` `===` `!==`
- bitový součin `&`
- bitový xor	`^`
- bitový součet	`|`
- logický and	`&&`
- logický or	`||`
- ternární operátor	`?:`
- přiřazení	`=` `+=` `-=` `*=` `/=` `%=` `<<=` `>>=` `>>>=` `&=` `^=` `|=`
- čárka	`,`

## Výrazy

- obyčejné
- s vedlejším efektem - např. přiřazení


- číselné, řetězcové, logické
- na levé straně - cíl přiřazení

## Comprehensions

- *připravovaná vlastnost*
- místo mapovací funkce

```javascript
[for (x of y) x] // pro pole
(for (x of y) y) // pro generátor

[for (i of [ 1, 2, 3 ]) i*i ]     // [ 1, 4, 9 ]
```

# Řídicí struktury

## Větvení `if` / `else`

- `else`, `else if` nepovinné

```javascript
if ( podmínka_1 ) {
    // příkazy
} else if ( podmínka_2 ) {
    // příkazy
} else if ( podmínka_n ) {
    // příkazy
} else {
    // příkazy
}
```

## Přiřazení v podmínce

```javascript
if (x == y) {} // porovnání
if (x = y) {}  // přiřazení
```
- `true` pokud `y` je `true` (výsledek přiřazení)
- obvykle se přidávají explicitní závorky

```javascript
if ((x = y)) {}
```

## Hodnoty `false`
- `false`, `undefined`, `null`, `NaN` (not a number)
- `0`, `''` (prázdný řetězec)

## Boolean objekt

```javascript
var b = new Boolean(false);
if (b) {}         // true!
if (b == true) {} // false
```

## Větvení `switch`

- program skočí na první `case` jehož hodnota vyhovuje výrazu
- pokud vynecháte `break` provedou se i následující `case`
- pokud žádný `case` nevyhovuje provede se `default` (pokud je uveden)

```javascript
switch ( výraz ) {
    case hodnota1:
        příkazy1;
        break;    // volitelně
    case hodnotaN:
        příkazyN;
        break;    // volitelně
    default:      // volitelně
        příkazyD;
        break;    // volitelně
}
```

## Cyklus `do` `while`

```javascript
// tělo cyklu se nemusí provéest ani jednou
while(i < 10) { console.log(i); i++ } // pro i == 1: 1 2 ... 9
```

```javascript
// tělo cyklu se alespoň jednou provede
do { console.log(i); i++ } while(i < 10); // pro i == 1: 1 2 ... 9
```

## Cyklus `for`

```javascript
for (var i = 1; i < 10; i++) { console.log(i); } // 1 2 ... 9
```
přes pole

```javascript
var a = [ 1, 1, 2, 3, 5, 8 ];
for (var i = 1; i < a.length; i++) { console.log(a[i]); } // 1 1 ... 8
```

## Cyklus [`for in`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration#for...in_statement)
přes pole

 ```javascript
 var a = [ 1, 1, 2, 3, 5, 8 ];
 for (var i in a) { console.log(a[i]); } // i obsahuje index (atribut)
 ```

přes atributy objektu

```javascript
var obj = {
    a: 10,
    b: 'ahoj'
};
for (prop in obj) {
    console.log(prop + ' = ' + obj[prop]);
}
```

## Cyklus [`for of`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration#for...of_statement)

jen přes iterovatelné položky

```javascript
var a = [ 1, 1, 2, 3, 5, 8 ];
a.ahoj = 'svete'

for (var i in a) { // i obsahuje index (atribut)
   console.log(i); // 0 ... 5 ahoj
}

for (var i of a) {
   console.log(i); // 1 1 ... 8
}
```

## Řízení cyklu
- `continue` - skok na začátek další iterace
- `continue label` - skok na začátek další iterace označeného cyklu
- `break` - skok ven z cyklu
- `break label` - skok ven z označeného cyklu

## Řízení cyklu - label

```javascript
var i = 1, j = 1;
label: while (true) { // označení cyklu
    console.log('i', i);
    i++;
    j = 1;
    while (true) {
        console.log('j', j);
        if(j > i) { console.log(j + ' > ' + i); break; }
        if(j == 10) { console.log('j==10'); break label;} // skok z označeného cyklu
        j++;
    }
}
```

## Zdroje

- [MDN Operators and Expressions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators)
- [MDN Control flow](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling)
- [MDN Loops and iteration](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration)
