// spusťte 02-main2.js

function Person(name) {
    this.name = name;
}

function Worker(name, dept) {
    Person.call(this, name);
    this.dept = dept;
}

Worker.prototype = Object.create(Person.prototype);

module.exports = {
    Person: Person,
    Worker: Worker
}
