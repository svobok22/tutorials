function addButtons(div) {
    // vytvoreni tlacitek
    var buttonAdd = $('<button>+</button>').addClass('btn btn-success');
    var buttonRemove = $('<button>-</button>').addClass('btn btn-danger');

    // zaregistrovani obsluhy
    buttonAdd.click(function() {
        addDiv(div);
    });

    buttonRemove.click(function() {
        removeDiv(div);
    });

    // vlozeni tlacitek do divu
    $(div)
        .append(buttonAdd)
        .append(buttonRemove)
    ;
}

function addDiv(root) {
    // vytvoreni elementu
    var el = $('<div></div>');
    el
        .html(Math.random())
        .mousemove(highlight)
    ;

    addButtons(el);

    // vlozeni potomka
    $(root).append(el);
}

function removeDiv(root) {
    $(root).children('div').last().remove();
}

function highlight(event) {
    var el = event.target;

    $(el).prev().css('color','red');
    $(el).css('color','black');
    $(el).next().css('color','blue');
}

$(document).ready(function() {
    $('div').each(function() {
        addButtons(this);
    });
});
