# Built-in funkce, regulární výrazy - cvičení

> [Teoretická část](../05-builtin.md)

## Řetězce

1. Napište funkci, která přehází slova v zadaném řetězci
    - rozdělte slovo podle mezery do pole, přeházejte prvky a následně je slepte to řetězce
    - použijte vhodné funkce pro práci s řetězcem a polem

## Regulární výrazy

1. Napište funkci, která ověří, zda je zadaný řetězec email.
1. Napište funkci, která ověří, zda je zadaný řetězec IPv4 adresa.
    - privátní
    - pokud ano, vrátí pole jednotlivých oktetů (bytů)
1. Napište funkci, která v řetězci nahradí bílé znaky pomlčkou
    - posloupnost bílých znaků nahraďte jedinou pomlčkou
    - odstraňte diakritiku

## Pole

1. Napište funkci, která nahradí opakující se hodnoty v poli hodnotou `[hodnota, počet]` např.

    ```
    [ 1, 2, 3, 3, 3, 2, 1, 4, 4, 2 ] ->
    [ 1, 2, [3, 3], 2, 1, [4, 2], 2]
    ```
2. Upravte předchozí úlohu tak, aby upravovala vstupní pole místo vytváření nového
3. **Rozšiřte myšlenku předchozí úlohy pro jednoduchou kompresi řetězce.**
   - pro vstupní řetězec generujte výstup tak, aby obsahoval opakující se znak jednou společně s informací o počtu opakování.
   - opakující se písmena zakódujte `@<počet opakování><písmeno>`, opakující se číslici `@<počet opakování>@<číslice>`
   - např. pro `aaaaa444444bbbcd3333225aaa` generujte výstup ve smyslu `@5a@6@4@3bcd@4@3225@3a`.
   - vypište kompresní poměr = délka komprimovaného řetězce / délka původního řetězce
   - zvažte jak dlouhé sekvence má smysl kódovat
   - volitelně navrhněte jiný způsob kódování
4. **Vytvořte funkci, která provede zpětný převod - dekompresi.**