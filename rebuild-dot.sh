#!/bin/bash

for i in img/*.dot; do
	echo "=== $i ==="
	dot -Tpng -O $i
done
